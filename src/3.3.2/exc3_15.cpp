
#include <iostream>
#include <vector>
#include <string>


int main(int argc, char *argv[])
{
	std::string str;
	std::vector<std::string> vec;

	while(getline(std::cin, str)) {
		if (str == std::string("1")) {
			break;
		}
		vec.push_back(str);
	}

	std::cout <<"vector size " << vec.size() <<std::endl;
	for (const auto &s : vec) {
		std::cout << "    " << s << std::endl;
	}

	return 0;
}
