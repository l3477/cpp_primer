

#include <iostream>
#include <string>

int main(int argc, char *argv[])
{
	std::string str;
	while(getline(std::cin, str)) {
		for (auto &c:str) {
			if (isalnum(c)) {
				c='X';
			}
		}
		std::cout << str << std::endl;
	}
	return 0;
}
