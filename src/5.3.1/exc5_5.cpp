

#include <iostream>
#include <string>

int int2str(int alph, std::string &str);


int main(int argc, char *argv[])
{
	if (argc < 2) {
		std::cout << "arg " << argc << " less 2!!"<< std::endl;
		return 1;
	}
	
	int grade=std::stoi(argv[1]);
	std::cout << "grade " << grade << std::endl;
	std::string grade_str;
	int ret;
	ret = int2str(grade, grade_str);
	if (ret == 0) {
		std::cout << "turn string grade " << grade_str << std::endl;
	}else{
		std::cout << "grade value" << grade << " incorrect" << std::endl;
	}
	return 0;
}

int int2str(int alph, std::string &str)
{
	if (alph > 999 || alph < 0) {
		return -1;
	}else if (alph > 99) {
		str.push_back(alph / 100 + '0');
		str.push_back(alph / 10 % 10+ '0');
		str.push_back(alph % 10 + '0');
	}
	else if (alph > 9) {
		str.push_back(alph / 10 + '0');
		str.push_back(alph % 10 + '0');
	}else{
		str.push_back(alph + '0');
	}
	return 0;
}
