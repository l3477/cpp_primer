

#include <iostream>
#include <vector>


int prefixcmp(const std::vector<int> &vec1, const std::vector<int> &vec2)
{
	int defsize=vec1.size() < vec2.size() ? vec1.size():vec2.size();
	for(int idx=0;idx < defsize;++idx){
		if (vec1[idx] != vec2[idx]) {
			return 0;
		}
	}
	return 1;
}

int main(void)
{
	std::vector<int> vec1={0,1,1,2};
	std::vector<int> vec2={0,1,1,2,3,5,8};
	std::cout << "vec 1 size "<< vec1.size() << " ,vec 2 size " << vec2.size() << std::endl;
	std::cout << "vec1 prefix compare with vec2 " << prefixcmp(vec1, vec2) << std::endl;
	return 0;
}
