

#include <iostream>
#include <stdexcept>

int main(void)
{
	int val1, val2;
	std::cin >> val1 >> val2;
	if (val2 == 0){
		throw  std::runtime_error("value2 must no be 0");
	}
	std::cout << val1 / val2 << std::endl;
	return 0;
}
