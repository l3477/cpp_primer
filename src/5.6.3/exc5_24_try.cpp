

#include <iostream>
#include <stdexcept>


int main(void)
{
	int val1, val2;
	while (std::cin >> val1 >> val2){
		try{
			if (val2 == 0) {
				throw std::runtime_error("value2 must not be 0");
			}
			std::cout << val1 / val2 << std::endl;
		}catch(std::runtime_error err) {
			std::cout << err.what() << std::endl;
			std::cout << "Try again? Enter y or n" << std::endl;
			int ch;
			std::cin >> ch;
			if (ch == 'n' || ch == 'N') {
				break;
			}
		}
	}
	return 0;
}
